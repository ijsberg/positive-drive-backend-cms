"use strict";

var verbose = false;
var logger = require('../share/logger.js')("tables/Prizes");

var table = module.exports = require('azure-mobile-apps').table();

table.read(function (context)
{
    var userID = context.user.id;
    if (context.user.claims.stable_sid)
        userID = context.user.claims.stable_sid;

    context.query.where("Available eq true or WonUserID eq '" + userID + "'");
    var ctx = context.execute();

    ctx.catch(function(ex)
    {
        logger.print("error", ex);
    });

    return ctx;
});

