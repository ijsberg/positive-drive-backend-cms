'use strict';

var verbose = false;
var logger = require('../share/logger.js')("tables/CampaignStat");
var queries = require('azure-mobile-apps/src/query');

var table = module.exports = require('azure-mobile-apps').table();

table.read(function (context) 
{
	var userID = context.user.id;
    if (context.user.claims.stable_sid)
        userID = context.user.claims.stable_sid;
    
	var campaignUserLinkTable = context.tables("CampaignUserLink");
	var q = campaignUserLinkTable.read(queries.create("CampaignUserLink").where({UserID: userID}));
	
	q.catch(function(ex)
	{
		logger.print("error", ex);
	});
	
	return q.then(function (result)
	{
		if (result.length > 0)
		{
			var w = "";
			var i = 0;
			result.forEach(function(item)
			{
				i++;
				w += "CampaignUserLinkID eq '" + item.id + "'";
				if (i < result.length)
				{
					w += " or ";
				}
			});
			
			if (verbose)
				logger.print("info", w);
			
			context.query.where(w);
			return context.execute();
		}
		else
		{
			context.query.where("CampaignUserLinkID eq 'EMPTY'");
			return context.execute();
		}
	});
});

// table.read.use(customMiddleware, table.operation);
