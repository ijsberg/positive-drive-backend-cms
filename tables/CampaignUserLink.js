'use strict';


var table = module.exports = require('azure-mobile-apps').table();

table.read(function (context) 
{
	var userID = context.user.id;
    if (context.user.claims.stable_sid)
        userID = context.user.claims.stable_sid;
    
	context.query.where({UserID: userID});
    return context.execute();
});

// table.read.use(customMiddleware, table.operation);
