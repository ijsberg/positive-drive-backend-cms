// ----------------------------------------------------------------------------
// Copyright (c) 2015 Microsoft Corporation. All rights reserved.
// ----------------------------------------------------------------------------
"use strict";
//catch uncaught exceptions, trace, then exit normally
process.on('uncaughtException', function(e) {
    console.log('Uncaught Exception...');
    console.log(e.stack);
    process.exit(99);
});

// Environment Config
const express         = require('express');
const expressLogger   = require('./share/express-logger');
const azureMobileApps = require('azure-mobile-apps');
const mobileApp  = azureMobileApps({
    // Explicitly enable the Azure Mobile Apps home page
    homePage: false,
    // Explicitly enable swagger support. UI support is enabled by
    // installing the swagger-ui npm module.
    swagger: false
});

// Set up a standard Express app
const bodyParser = require('body-parser');
const app        = express();
const Logger     = require('./share/logger')('app.js');

app.use(expressLogger(Logger));

// Add AppInsights
if ( process.env.APPINSIGHTS_INSTRUMENTATIONKEY ) {
    const appInsights     = require("applicationinsights");

    appInsights.setup(process.env.APPINSIGHTS_INSTRUMENTATIONKEY)
        .setAutoDependencyCorrelation(true)
        .setAutoCollectRequests(true)
        .setAutoCollectPerformance(true)
        .setAutoCollectExceptions(true)
        .setAutoCollectDependencies(true)
        .start();

    app.use((req, res, next) => {
        appInsights.client.trackRequest(req,res);
        next();
    });
}
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));


mobileApp.tables.import('./tables');
mobileApp.api.import('./api');
mobileApp.tables.initialize()
    .then(function () {
        app.use(mobileApp);    // Register the Azure Mobile Apps middleware
        app.get('/health', function(req, res){
            res.send('OK')
        });
        app.listen(process.env.PORT || 3000, function(){
            console.info("Running Server at port " + (process.env.PORT || 3000));
        });   // Listen for requests
        process.setMaxListeners(10000);
    },
    function (error){
        console.info(error);
    }
);
