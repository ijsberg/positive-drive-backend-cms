
"use strict";
const verbose = true;
const ContentType = 'application/json';

function reprocess(context, id){
    if( !id ){
        return ({ status: 500, payload: {
            success: false,
            error: `Failed to get item from table ${TableName} - no Id defined'`
        }})
    }
    return Promise.all([context.data.execute({sql: `
                UPDATE ProcessedRouteDetails
                SET ProcessedState = 0, deleted = 0
                WHERE id = '${id}'`
            }), context.data.execute({sql: `
                UPDATE rrs
                SET ProcessedState = -1, Processed = 0, ProcessLock = 0, deleted = 0
                FROM RawRouteSummary rrs
                  INNER JOIN ProcessedRouteDetails prd
                  ON rrs.id = prd.RawRouteSummaryID
                  WHERE prd.id = '${id}'`
            }), context.data.execute({sql: `
                UPDATE rrw
                SET
                  rrw.Processed = 0,
                  rrw.deleted = 0
                FROM RawRouteWaypoints rrw
                  INNER JOIN ProcessedRouteDetails prd
                  ON rrw.GUID = prd.GUID
                WHERE rrw.Timestamp >= prd.StartTime AND rrw.Timestamp <= prd.StopTime AND prd.id = '${id}'`
            })]).then(
                results => {
                    return { status: 200, payload: {
                        success: true,
                        data   : results[0]
                    }}
                },
                error   => ({ status: 500, payload: {
                    success: false,
                    error: `Failed to query table '${TableName}' for Id '${id}'`
            }})
        );
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}

const cms = {
    get: function (req, res)
    {
        let context     = req.azureMobile;
        let id          = req.query.id;

        reprocess(context, id).then(toResponse(res));
    }
};

module.exports = cms;
