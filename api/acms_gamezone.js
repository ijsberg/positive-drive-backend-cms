
"use strict";
const verbose = true;
const DefaultPageSize = 20;
const DefaultOffset   =  0;
const DefaultOrderBy  = 'createdAt';
const TableName       = 'Gamezone';
const ContentType     = 'application/json';
const UUID            = require('uuid');

function getList(context, offset, pageSize, orderBy){
    offset   = offset   || DefaultOffset;
    pageSize = pageSize || DefaultPageSize;
    orderBy  = orderBy  || DefaultOrderBy;

    return Promise.all([
        context.data.execute({sql: `
            SELECT COUNT(*) as total 
            FROM Gamezone 
            WHERE deleted=0
        `}), // totalCount,
        // Already excludes the deleted ones
        context.data.execute({sql: `
            SELECT
                id,
                updatedAt,
                version,
                deleted,
                Name,
                DistancePerSmile_walk / 1000 AS DistancePerSmile_walk,
                DistancePerSmile_bike / 1000 AS DistancePerSmile_bike,
                DistancePerSmile_car / 1000 AS DistancePerSmile_car,
                DistancePerSmile_bus / 1000 AS DistancePerSmile_bus,
                Geometry,
                Priority
            FROM Gamezone
            WHERE deleted=0
            ORDER BY updatedAt DESC
        `})
    ]).then(
        data => ({ status:200, payload:{
            success: true,
            count  : data[0][0].total,
            data   : data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table ${TableName}`
        }})
    );
}

function getById(context, id){
    if( !id ){
        return ({ status: 500, payload: {
            success: false,
            error: `Failed to get item from table ${TableName} - no Id defined'`
        }})
    }
    return context.tables(TableName).where({ id: id }).read().then(
        results => {
            if( results.length === 0 ){
                return { status : 404, payload: {
                    success: false,
                    error  : `Item with Id '${id}' not found in table '${TableName}'`
                }}
            }else{
                return { status: 200, payload: {
                    success: true,
                    data   : results[0]
                }}
            }
        },
        error   => ({ status: 500, payload: {
            success: false,
            error: `Failed to query table '${TableName}' for Id '${id}'`
        }})
    );
}

function create(context, item){
    item.id = UUID.v4();
    return context.tables(TableName).insert(item).then(
        result => ({ status: 200, payload: {
            success: true,
            data   : result
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Failed to create item on Table '${TableName}' with data '${JSON.stringify(item)}'`
        }})
    );
}

function update(context, item){
    return context.tables(TableName).update( item ).then(
        results => ({ status: 200, payload: {
            success: true,
            data   : item
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Failed to update item on Table '${TableName}' with data '${JSON.stringify(item)}'`
        }})
    );
}

function remove(context, id){
    if( !id ){
        return ({ status: 500, payload: {
            success: false,
            error: `Failed to delete item from table ${TableName} - no Id defined'`
        }})
    }
    return context.data.execute({sql: `UPDATE ${TableName} SET deleted=1 WHERE id = '${id}';`}).then(
        results => ({ status: 200, payload: {
            success: true
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Failed to delete item from Table '${TableName}' with id '${id}'`
        }})
    );
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}
const cms = {
    get: function (req, res)
    {
        let context    = req.azureMobile;
        let gamezoneId = req.query.id;
        let offset     = parseInt(req.query.offset);
        let pageSize   = parseInt(req.query.pageSize);
        let orderBy    = req.query.orderBy;

        (gamezoneId ? getById(context, gamezoneId) : getList(context, offset, pageSize, orderBy))
            .then( toResponse(res) );
    },
    post: function(req, res) {
        let context = req.azureMobile;
        let item    = req.body;
        (item.id ? update(context, item) : create(context, item)).then( toResponse(res) );
    },
    delete: function(req, res) {
        let context    = req.azureMobile;
        let gamezoneId = req.query.id;

        remove(context, gamezoneId).then( toResponse(res) );
    }
};

module.exports = cms;
