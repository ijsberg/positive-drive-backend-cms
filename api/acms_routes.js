
"use strict";
const verbose = true;
const TableName = 'ProcessedRouteDetails';
const ContentType = 'application/json';
const DefaultPageSize = 20;
const DefaultOffset   =  0;

function getList(context, offset, pageSize, orderBy){
    return Promise.all([
        context.data.execute({sql: `
            SELECT COUNT(*) total
            FROM ProcessedRouteDetails rd
                INNER JOIN UserDevice ud ON ud.GUID   = rd.GUID
                INNER JOIN Users      us ON ud.UserID = us.id
            WHERE rd.deleted = 0
        `}), // totalCount,
        // Already excludes the deleted ones
        context.data.execute({sql: `
            SELECT
              data.id,
              CONCAT(data.FirstName, ' ', data.LastName) as Name,
              data.StartTime,
              data.StopTime,
              data.AverageSpeed,
              data.Modality,
              data.TotalDistance / 1000 TotalDistance,
              data.FromDescription,
              data.ToDescription,
              (
                SELECT STRING_AGG(Name, ', ')
                FROM Gamezone
                WHERE CHARINDEX(id, data.CrossoverGamezones) > 0
              ) as GamezoneName,
              data.SmilesEarnedByDistance,
              data.SmilesEarnedByPois,
              data.PoisHit,
              data.TripID,
              (CASE
               WHEN data.CheckEvaded = 0
                 THEN 'Not in a campaign'
               WHEN data.BadgesEarned LIKE '%{type:"campaign"}%'
                 THEN 'Successful Campaign route'
               ELSE 'Failed Campaign route'
               END
              ) as CampaignRouteResult,
              data.ProcessedState
            FROM (
                SELECT prd.*, us.FirstName, us.LastName
                FROM ProcessedRouteDetails prd
                  INNER JOIN UserDevice ud ON ud.GUID   = prd.GUID
                  INNER JOIN Users      us ON ud.UserID = us.id
                WHERE prd.deleted = 0
                ORDER BY prd.StartTime DESC
                OFFSET ${offset} ROWS
                FETCH NEXT ${pageSize} ROWS ONLY
              )AS data
            ORDER BY data.StartTime DESC
        `})
    ]).then(
        data => ({ status:200, payload:{
            success: true,
            count  : data[0][0].total,
            data   : data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table ${TableName}`
        }})
    );
}

function getById(context, id){
    if( !id ){
        return ({ status: 500, payload: {
            success: false,
            error: `Failed to get item from table ${TableName} - no Id defined'`
        }})
    }
    return context.data.execute({sql: `
        SELECT
          CONCAT(us.FirstName, ' ', us.LastName) as Name,
          usd.Model                              as Model,
          usd.AppVersion                         as AppVersion,
          prd.StartTime                          as StartTime,
          prd.StopTime                           as StopTime,
          prd.ProcessedState                     as ProcessedState,
          (
            SELECT COUNT(*)
            FROM RawRouteWaypoints rrw
            WHERE rrw.Timestamp >= prd.StartTime AND rrw.Timestamp <= prd.StopTime AND rrw.GUID = prd.GUID
          ) as PointsCollected,
          prd.TotalDistance / 1000 as TotalDistance,
          (
            CASE
              WHEN prd.RawRouteSummaryID IS NOT NULL
                THEN CAST(1 AS BIT)
            ELSE     CAST(0 AS BIT)
            END
          ) as HasRawRouteSummary,
          rrs.ProcessedState as RawRouteProcessedState,
          rrs.ProcessLock    as RawRouteProcessLock,
          rrs.GUID           as GUID,
          usd.RoutenetID     as RoutenetID
        FROM ProcessedRouteDetails prd
          INNER JOIN UserDevice usd ON prd.GUID = usd.GUID
          INNER JOIN Users      us  ON us.id    = usd.UserID
          LEFT  JOIN RawRouteSummary rrs ON prd.RawRouteSummaryID = rrs.id
        WHERE prd.id = '${id}'
    `}).then(
        results => {
            if( results.length === 0 ){
                return { status : 404, payload: {
                    success: false,
                    error  : `Item with Id '${id}' not found in table '${TableName}'`
                }}
            }else{
                return { status: 200, payload: {
                    success: true,
                    data   : results[0]
                }}
            }
        },
        error   => ({ status: 500, payload: {
            success: false,
            error: `Failed to query table '${TableName}' for Id '${id}'`
        }})
    );
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}
const cms = {
    get: function (req, res)
    {
        let context     = req.azureMobile;
        let id          = req.query.id;
        let offset      = parseInt(req.query.offset)   || DefaultOffset;
        let pageSize    = parseInt(req.query.pageSize) || DefaultPageSize;
        let orderBy     = req.query.orderBy;

        (id ? getById(context, id) : getList(context, offset, pageSize, orderBy)).then( toResponse(res) );
    }
};

module.exports = cms;
