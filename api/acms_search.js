'use strict';
const verbose = true;
const ContentType = 'application/json';
const UUID        = require('uuid');

function getList(context, userId){
    return Promise.all([
        context.data.execute({sql: `
            SELECT COUNT(*) total
            FROM AspNetUsersSearch 
            WHERE UserId = '${userId}'
        `}), // totalCount,
        // Already excludes the deleted ones
        context.data.execute({sql: `
            SELECT  id, Name, Search
            FROM AspNetUsersSearch 
            WHERE UserId = '${userId}'
        `})
    ]).then(
        data => ({ status:200, payload:{
            success: true,
            count  : data[0][0].total,
            data   : data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table ${TableName}`
        }})
    );
}


function createSearch(context, item){
    return context.data.execute({sql: `
        INSERT AspNetUsersSearch (id, UserId, Name, Search)
        VALUES ('${item.id}', '${item.UserId}', '${item.Name}', '${item.Search}') 
    `}).then(
        data => ({ status:200, payload:{
            success: true,
            count  : 1,
            data   : item
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table ${TableName}`
        }})
    );
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}

const api = {
    get: function (req, res) {
        let context = req.azureMobile;
        let userID = context.user.id;

        getList(context, userID).then(toResponse(res))
    },
    post: function(req, res){
        let context    = req.azureMobile;
        let userID     = context.user.id;
        let searchName = req.body.name;
        let searchData = req.body.data;
        let item = {
            id     : UUID.v4(),
            UserId : userID,
            Name   : searchName,
            Search : JSON.stringify(searchData)
        };
        createSearch(context, item).then(toResponse(res))
    }
};

module.exports = api;
