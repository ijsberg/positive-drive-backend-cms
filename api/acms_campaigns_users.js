
"use strict";
const verbose = true;
const DefaultPageSize  = 20;
const DefaultOffset    =  0;
const DefaultOrderBy   = 'createdAt';
const ContentType      = 'application/json';

function getById(context, id, offset, pageSize){
    if( !id ){
        return Promise.resolve({ status: 500, payload: {
            success: false,
            error: `Failed to get Campaign Users - No Id defined.`
        }})
    }

    return Promise.all([
        context.data.execute({sql: `
            SELECT COUNT(*) as total
            FROM CampaignUserLink   as cul
              INNER JOIN Users      as usr ON usr.id = cul.UserID
            WHERE CampaignID = '${id}' AND usr.deleted  = 0
        `}),
        context.data.execute({sql: `
            SELECT
              usr.id                                                          as id,
              CONCAT(usr.FirstName, ' ', usr.LastName)                        as name,
              usr.Email                                                       as email,
              (select g.Name from Gamezone g WHERE g.id = usr.LastGamezoneID) as lastGamezoneName,
              usr.Provider                                                    as provider,
              div.Model                                                       as phoneType,
              div.RoutenetID                                                  as routenetId
            FROM CampaignUserLink   as cul
              INNER JOIN Users      as usr ON usr.id = cul.UserID
              INNER JOIN UserDevice as div ON usr.id = div.UserID
            WHERE CampaignID = '${id}'
              AND usr.deleted  = 0
            ORDER BY name
            OFFSET ${offset} ROWS
            FETCH NEXT ${pageSize} ROWS ONLY
        `})
    ]).then(
        data => ({ status:200, payload:{
            success: true,
            count  : data[0][0].total,
            data   : data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to execute query - CampaignUsers`
        }})
    );
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    };
}

const cms = {
    get: function (req, res)
    {
        let context    = req.azureMobile;
        let gamezoneId = req.query.id;
        let offset     = parseInt(req.query.offset  ) || DefaultOffset;
        let pageSize   = parseInt(req.query.pageSize) || DefaultPageSize;

        getById(context, gamezoneId, offset, pageSize).then( toResponse(res) );
    }
};

module.exports = cms;
