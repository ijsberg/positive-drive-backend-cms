'use strict';
const verbose = true;
const identity = require('aspnet-identity-pw');
const logger   = require('../share/logger.js')("api/acms_users_changePassword");

var api = 
{
    post: function (req, res) {

        let context = req.azureMobile;
        let userID  = context.user.id;

        if (context.user.claims.stable_sid){
            userID = context.user.claims.stable_sid;
        }
        let data = req.query;
        if ( !data || Object.keys(data).length === 0 ){
            data = req.body;
        }

        let oldPassword = data.OldPassword;
        let newPassword = data.NewPassword;

        let userTable = context.tables("AspNetUsers");
        userTable.where({id: userID}).read().then(function(result) {
            if (result.length === 0) {
                logger.print("error", "Attempted password reset of invalid user " + userID);
                res.status(500).send("No user found");
                return;
            }
            
            let user = result[0];

            if (identity.validatePassword(oldPassword, user.PasswordHash)) {
                user.PasswordHash = identity.hashPassword(newPassword);

                userTable.update(user).then(() =>
                {
                    res.status(200).send({success: true})
                }).catch((ex) =>
                {
                    logger.print("error", ex);
                    res.status(500).send(ex);
                });
            } else {
                res.status(200).send({success: false});
            }
        }).catch(function(ex) {
            logger.print("error", ex);
            res.status(500).send(ex);
        });
    }
};

module.exports = api;
