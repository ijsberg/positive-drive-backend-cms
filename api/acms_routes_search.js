
"use strict";
const verbose = true;
const Moment            = require('moment');
const DefaultDateFormat = 'YYYY-MM-DD';
const ContentType       = 'application/json';
const DefaultPageSize   = 20;
const DefaultOffset     =  0;

function getSearch(context, offset, pageSize, query){
    let whereFilters = [
        'prd.deleted=0'
    ];

    if( query.searchFor && query.searchFor.trim().length > 0 ){
        whereFilters.push(' (' + [
            `prd.FromDescription LIKE '%${query.searchFor.trim()}%'`,
            `prd.ToDescription   LIKE '%${query.searchFor.trim()}%'`
        ].join(' OR ') + ') ')
    }
    if( query.dateRange && query.dateRange.type === 'custom' ){
        let startDate = Moment(query.dateRange.start, "YYYY/MM/DD").format(DefaultDateFormat);
        let endDate   = Moment(query.dateRange.end  , "YYYY/MM/DD").format(DefaultDateFormat);

        whereFilters.push(` prd.StartTime >= '${startDate}' ` );
        whereFilters.push(` prd.StopTime  <= '${endDate}'   ` );
    }else if (query.dateRange && query.dateRange.type === 'days'){
        let startDate = Moment().subtract(query.dateRange.days, 'days').format(DefaultDateFormat);

        whereFilters.push(` prd.StartTime >= '${startDate}' ` );
    }

    if( query.routeState && query.routeState.trim().length > 0 ){
        whereFilters.push(` prd.ProcessedState = ${query.routeState}`);
    }
    if( query.routeResult instanceof Object ){
        if ( query.routeResult.routeCheckEvaded && query.routeResult.routeCheckEvaded.trim().length > 0 ){
            whereFilters.push(` prd.CheckEvaded = ${query.routeResult.routeCheckEvaded}`);
        }else if ( query.routeResult.routeBadgesEarned && query.routeResult.routeBadgesEarned.trim().length > 0 ){
            whereFilters.push(` prd.BadgesEarned LIKE '%${query.routeResult.routeBadgesEarned}%'`);
        }else {
            whereFilters.push(` prd.CheckEvaded <> 0`);
            whereFilters.push(` (prd.BadgesEarned NOT LIKE '%{type:"campaign"}%' OR prd.BadgesEarned IS NULL) `);
        }
    }

    if( query.routeModality && query.routeModality.trim().length > 0 ){
        whereFilters.push(` prd.Modality = ${query.routeModality}`);
    }
    return Promise.all([
        context.data.execute({
            sql: `
            SELECT COUNT(*) total
            FROM ProcessedRouteDetails prd
                INNER JOIN UserDevice ud ON ud.GUID   = prd.GUID
                INNER JOIN Users      us ON ud.UserID = us.id
            WHERE ${whereFilters.join(' AND ')}
            `
        }), // totalCount,
        // Already excludes the deleted ones
        context.data.execute({sql: `
            SELECT
              data.id,
              CONCAT(data.FirstName, ' ', data.LastName) as Name,
              data.StartTime,
              data.StopTime,
              data.AverageSpeed,
              data.Modality,
              data.TotalDistance / 1000 TotalDistance,
              data.FromDescription,
              data.ToDescription,
              (
                SELECT STRING_AGG(Name, ', ')
                FROM Gamezone
                WHERE CHARINDEX(id, data.CrossoverGamezones) > 0
              ) as GamezoneName,
              data.SmilesEarnedByDistance,
              data.SmilesEarnedByPois,
              data.PoisHit,
              data.TripID,
              (CASE
               WHEN data.CheckEvaded = 0
                 THEN 'Not in a campaign'
               WHEN data.BadgesEarned LIKE '%{type:"campaign"}%'
                 THEN 'Successful Campaign route'
               ELSE 'Failed Campaign route'
               END
              ) as CampaignRouteResult,
              data.ProcessedState
            FROM (
               SELECT prd.*, us.FirstName, us.LastName
               FROM ProcessedRouteDetails prd
                 INNER JOIN UserDevice ud ON ud.GUID   = prd.GUID
                 INNER JOIN Users      us ON ud.UserID = us.id
               WHERE ${whereFilters.join(' AND ')}
               ORDER BY prd.StartTime DESC
                 OFFSET ${offset} ROWS
                 FETCH NEXT ${pageSize} ROWS ONLY
             )AS data
            ORDER BY data.StartTime DESC
        `})
    ]).then(
        data => ({ status:200, payload:{
            success: true,
            count  : data[0][0].total,
            data   : data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table Routes`
        }})
    );
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}
const cms = {
    post: function (req, res)
    {
        let context    = req.azureMobile;
        let query      = req.body;
        let offset     = parseInt(req.query.offset)   || DefaultOffset;
        let pageSize   = parseInt(req.query.pageSize) || DefaultPageSize;

        getSearch(context, offset, pageSize, query).then(toResponse(res));
    }
};

module.exports = cms;
