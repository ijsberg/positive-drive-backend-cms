"use strict";
const verbose = true;
const ContentType   = 'application/json';
const Moment        = require('moment');
const DefaultDateFormat = 'YYYY-MM-DD';

function getSearch(context, query){

    let whereFilters = [
        'cp.deleted=0'
    ];

    if( query.searchFor && query.searchFor.trim().length > 0 ){
        whereFilters.push(` ( cp.Name LIKE '%${query.searchFor.trim()}%' OR gz.Name LIKE '%${query.searchFor.trim()}%' ) `)
    }
    if( query.dateRange && query.dateRange.type === 'custom' ){
        let startDate = Moment(query.dateRange.start, "YYYY/MM/DD").format(DefaultDateFormat);
        let endDate   = Moment(query.dateRange.end  , "YYYY/MM/DD").format(DefaultDateFormat);

        whereFilters.push(` cp.StartDate >= '${startDate}' ` );
        whereFilters.push(` cp.StartDate <= '${endDate}'   ` );
    }else if (query.dateRange && query.dateRange.type === 'days'){
        let startDate = Moment().subtract(query.dateRange.days, 'days').format(DefaultDateFormat);

        whereFilters.push(` cp.StartDate >= '${startDate}' ` );
    }

    (query.modalities || []).filter(_ => !!_).forEach( x => {
        whereFilters.push(` cp.Modalities LIKE '%|%${x}%' `)
    });

    (query.avoidance || []).filter(_ => !!_).forEach( x => {
        whereFilters.push(` ( cp.Modalities LIKE '%${x}%|%' OR cp.Modalities LIKE '%${x}%' AND cp.Modalities NOT LIKE '%|%' )`)
    });

    return Promise.all([
        context.data.execute({
            sql: `
            SELECT COUNT(*) as total 
            FROM Campaigns cp
              INNER JOIN Gamezone gz
              ON cp.GamezoneLink = gz.id 
            WHERE ${whereFilters.join(' AND ')}
            `
        }), // totalCount,
        // Already excludes the deleted ones
        context.data.execute({sql: `
            SELECT
              cp.id          Id,
              cp.Name        Name,
              cp.Modalities  Modalities,
              cp.ActiveTimes ActiveTimes,
              cp.Type        Type,
              cp.StartDate   StartDate,
              gz.Name        GamezoneName
            FROM Campaigns  cp
              INNER JOIN Gamezone gz
              ON cp.GamezoneLink = gz.id 
            WHERE ${whereFilters.join(' AND ')}
        `})
    ]).then(
        data => ({ status:200, payload:{
            success: true,
            count  : data[0][0].total,
            data   : data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table Campaign`
        }})
    );
}
/**
 * Processes the Http response to be sent
 *
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}

const cms = {
    post: function(req, res){
        let context    = req.azureMobile;
        let query      = req.body;

        getSearch(context, query).then(toResponse(res));
    }
};

module.exports = cms;
