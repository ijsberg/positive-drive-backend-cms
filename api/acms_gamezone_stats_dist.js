"use strict";
const verbose = true;
const AbstractStats = require("../share/abstractStats");
const ContentType   = 'application/json';
const DefaultStat   = {
    total: 0,
    walk:  0,
    bike:  0,
    bus:   0,
    car:   0
};


class GamezoneOverallStats extends AbstractStats{
    constructor(){
        super(DefaultStat, 'Gamezone Stats Distance')
    }

    buildGetByIdQuery(id, todayDate, pastDate) {
        return `
        SELECT updatedAt as date,
          sum(car + bus + bike + walk) as total,
          sum(car) as car,
          sum(bus) as bus,
          sum(bike) as bike,
          sum(walk) as walk
        FROM (
          (
            SELECT CONVERT(date, stats.updatedAt, 1) as updatedAt, CurrentValue as bike, 0 as bus, 0 as car, 0 as walk
            FROM Stats_KMBike stats INNER JOIN GamezoneUserLink gul ON stats.GamezoneUserLinkID = gul.id
            WHERE 
              stats.updatedAt >= '${pastDate}'  AND
              stats.updatedAt <= '${todayDate}' AND 
              gul.GamezoneID   = '${id}'
          ) UNION (
            SELECT CONVERT(date, stats.updatedAt, 1) as updatedAt, 0 as bike, CurrentValue as bus, 0 as car, 0 as walk
            FROM Stats_KMBus stats INNER JOIN GamezoneUserLink gul ON stats.GamezoneUserLinkID = gul.id
            WHERE 
              stats.updatedAt >= '${pastDate}'  AND
              stats.updatedAt <= '${todayDate}' AND 
              gul.GamezoneID   = '${id}'
          ) UNION (
            SELECT CONVERT(date, stats.updatedAt, 1) as updatedAt, 0 as bike, 0 as bus, CurrentValue as car, 0 as walk
            FROM Stats_KMCar stats INNER JOIN GamezoneUserLink gul ON stats.GamezoneUserLinkID = gul.id 
            WHERE 
              stats.updatedAt >= '${pastDate}'  AND
              stats.updatedAt <= '${todayDate}' AND 
              gul.GamezoneID   = '${id}'
          ) UNION (
            SELECT CONVERT(date, stats.updatedAt, 1) as updatedAt, 0 as bike, 0 as bus, 0 as car, CurrentValue as walk
            FROM Stats_KMWalk stats INNER JOIN GamezoneUserLink gul ON stats.GamezoneUserLinkID = gul.id
            WHERE 
              stats.updatedAt >= '${pastDate}'  AND
              stats.updatedAt <= '${todayDate}' AND 
              gul.GamezoneID   = '${id}'
          )
        ) as stats
        GROUP BY updatedAt
        ORDER BY updatedAt DESC`;
    }

    buildGetQuery(todayDate, pastDate) {
        return  `
        SELECT updatedAt as date,
          sum(car + bus + bike + walk) as total,
          sum(walk) as walk,
          sum(bike) as bike,
          sum(bus) as bus,
          sum(car) as car
        FROM (
          (
            SELECT CONVERT(date, updatedAt, 1) as updatedAt, CurrentValue as bike, 0 as bus, 0 as car, 0 as walk
            FROM Stats_KMBike
            WHERE 
              updatedAt >= '${pastDate}'  AND
              updatedAt <= '${todayDate}'
          ) UNION (
            SELECT CONVERT(date, updatedAt, 1) as updatedAt, 0 as bike, CurrentValue as bus, 0 as car, 0 as walk
            FROM Stats_KMBus
            WHERE 
              updatedAt >= '${pastDate}'  AND
              updatedAt <= '${todayDate}'
          ) UNION (
            SELECT CONVERT(date, updatedAt, 1) as updatedAt, 0 as bike, 0 as bus, CurrentValue as car, 0 as walk
            FROM Stats_KMCar
            WHERE 
              updatedAt >= '${pastDate}'  AND
              updatedAt <= '${todayDate}'
          ) UNION (
            SELECT CONVERT(date, updatedAt, 1) as updatedAt, 0 as bike, 0 as bus, 0 as car, CurrentValue as walk
            FROM Stats_KMWalk
            WHERE 
              updatedAt >= '${pastDate}'  AND
              updatedAt <= '${todayDate}'
          )
        ) as stats
        GROUP BY updatedAt
        ORDER BY updatedAt DESC`;
    }
}

const statsBuilder = new GamezoneOverallStats();

/**
 * Processes the Http response to be sent
 *
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}

const cms = {
    get: function(req, res){
        let context    = req.azureMobile;
        let id         = req.query.id;
        let startDate  = req.query.startDate;
        let dayRange   = parseInt(req.query.dayRange);

        statsBuilder.getStats(context, id, dayRange, startDate).then( toResponse(res) );
    }
};

module.exports = cms;

