"use strict";
const verbose = true;
const AbstractStats = require("../share/abstractStats");
const ContentType   = 'application/json';
const DefaultStat   = {
    processing: 0,
    total:      0,
    walk:       0,
    bike:       0,
    run:        0,
    car:        0,
    transit:    0
};

class GamezoneStats extends AbstractStats{
    constructor(){
        super(DefaultStat, 'Gamezone Overall')
    }

    buildGetByIdQuery(id, todayDate, pastDate) {
        return `
        SELECT
          prd.updatedAt                 as date,
          sum(prd.processing)           as processing,
          sum(rrs.Stationary)           as transit,
          sum(rrs.Walk)                 as walk,
          sum(rrs.Run)                  as run,
          sum(rrs.Bike)                 as bike,
          sum(rrs.Car)                  as car,
          sum(processing + stationary + walk + run + bike + car) as total
        FROM (
           SELECT
             CONVERT(date, updatedAt, 1) as updatedAt,
             RawRouteSummaryID           as id,
             CASE ProcessedState
               WHEN 1 THEN 0
               ELSE 1 END                as processing
           FROM ProcessedRouteDetails
           WHERE 
             CrossoverGamezones LIKE '%${id}%' 
             AND updatedAt      >=   '${pastDate}'
             AND updatedAt      <=   '${todayDate}'
        ) AS prd
        INNER JOIN RawRouteSummary AS rrs ON prd.id = rrs.id
        GROUP BY prd.updatedAt
        ORDER BY date DESC`;
    }

    buildGetQuery(todayDate, pastDate) {
        return  `
        SELECT
          prd.updatedAt                 as date,
          sum(prd.processing)           as processing,
          sum(rrs.Stationary)           as transit,
          sum(rrs.Walk)                 as walk,
          sum(rrs.Run)                  as run,
          sum(rrs.Bike)                 as bike,
          sum(rrs.Car)                  as car,
          sum(processing + stationary + walk + run + bike + car) as total
        FROM (
           SELECT
             CONVERT(date, updatedAt, 1) as updatedAt,
             RawRouteSummaryID           as id,
             CASE ProcessedState
                 WHEN 1 THEN 0
                 ELSE 1 END           as processing
           FROM ProcessedRouteDetails
           WHERE 
             updatedAt >= '${pastDate}' AND 
             updatedAt <= '${todayDate}'
        ) AS prd
        INNER JOIN RawRouteSummary AS rrs ON prd.id = rrs.id
        GROUP BY prd.updatedAt
        ORDER BY date DESC`;
    }
}

const statsBuilder = new GamezoneStats();



/**
 * Processes the Http response to be sent
 *
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}

const cms = {
    get: function(req, res){
        let context    = req.azureMobile;
        let id         = req.query.id;
        let startDate  = req.query.startDate;
        let dayRange   = parseInt(req.query.dayRange);

        statsBuilder.getStats(context, id, dayRange, startDate).then( toResponse(res) );
    }
};

module.exports = cms;
