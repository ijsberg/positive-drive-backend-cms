
"use strict";
const verbose = true;
const ContentType     = 'application/json';
const DefaultPageSize  = 20;
const DefaultOffset    =  0;

/**
 *
 * @param context
 * @param offset
 * @param pageSize
 * @param query {{searchFor:string, gamezoneId:string}}
 * @return {Promise.<{status: number, payload: {success: boolean, count, data}}>}
 */
function getList(context, offset, pageSize, query){
    let whereFilters = [
        'usr.deleted = 0'
    ];
    let subWhereFilter = [
        'deleted = 0',
        'WonUserID IS NOT NULL'
    ];

    if( query.searchFor && query.searchFor.trim().length > 0 ){
        whereFilters.push(' (' + [
            `usr.FirstName LIKE '%${query.searchFor.trim()}%'`,
            `usr.LastName  LIKE '%${query.searchFor.trim()}%'`,
            `usr.Email     LIKE '%${query.searchFor.trim()}%'`,
            `usr.Provider  LIKE '%${query.searchFor.trim()}%'`
        ].join(' OR ') + ') ')
    }
    if (query.gamezoneId && query.gamezoneId.trim().length > 0){
        subWhereFilter.push(`GamezoneID = '${query.gamezoneId}'`)
    }

    return Promise.all([
        context.data.execute({sql: `
            SELECT count(*) as total
            FROM (
                SELECT WonUserID
                FROM Prizes_V2 as pr1 INNER JOIN (
                  SELECT DISTINCT Name, Company, GamezoneID
                  FROM Prizes_V2 
                  WHERE ${subWhereFilter.join(' AND ')}
                ) AS pr2 ON pr1.Name = pr2.Name AND pr1.Company = pr2.Company AND pr1.GamezoneID = pr2.GamezoneID
            ) as pr INNER JOIN Users usr ON usr.id = pr.WonUserID
            WHERE ${whereFilters.join(' AND ')}
        `}), // totalCount,
        // Already excludes the deleted ones
        context.data.execute({sql: `
            SELECT
              usr.id                                   id,
              CONCAT(usr.FirstName, ' ', usr.LastName) name,
              usr.Email                                email,
              FORMAT(pr.WonDate, 'dd/MM/yyyy hh:mm')  date,
              pr.ClaimAddress                         claimAddress,
              pr.Claimed                              claimed,
              pr.Codes                                codes,
              gz.Name                                 gamezoneName                                 
            FROM (
              SELECT WonUserID, ClaimAddress, Claimed, Codes, WonDate, DisplayName, pr1.Name, pr1.GamezoneID 
              FROM Prizes_V2 as pr1 INNER JOIN (
                SELECT DISTINCT Name, Company, GamezoneID
                FROM Prizes_V2
                WHERE ${subWhereFilter.join(' AND ')}
              ) AS pr2 ON pr1.Name = pr2.Name AND pr1.Company = pr2.Company AND pr1.GamezoneID = pr2.GamezoneID
            ) as pr 
              INNER JOIN Users usr ON usr.id = pr.WonUserID
              INNER JOIN Gamezone gz ON gz.id = pr.GamezoneID
            WHERE ${whereFilters.join(' AND ')}
            ORDER BY name
            OFFSET ${offset} ROWS
            FETCH NEXT ${pageSize} ROWS ONLY
        `
        })
    ]).then(
        data => ({ status:200, payload:{
            success: true,
            count  : data[0][0].total,
            data   : data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table Prize Winners Search`
        }})
    );
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}
const cms = {
    post: function (req, res)
    {
        let context    = req.azureMobile;
        let offset     = parseInt(req.query.offset  ) || DefaultOffset;
        let pageSize   = parseInt(req.query.pageSize) || DefaultPageSize;
        let query      = req.body;
        getList(context, offset, pageSize, query).then( toResponse(res) );
    }
};

module.exports = cms;
