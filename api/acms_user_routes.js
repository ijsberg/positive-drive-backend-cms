
"use strict";
const verbose = true;
const DefaultPageSize = 20;
const DefaultOffset   =  0;
const DefaultOrderBy  = 'createdAt';
const TableName       = 'Users';
const ContentType     = 'application/json';

function getById(context, offset, pageSize, orderBy, id){
    offset   = offset   || DefaultOffset;
    pageSize = pageSize || DefaultPageSize;
    orderBy  = orderBy  || DefaultOrderBy;

    return Promise.all([
        context.data.execute({sql: `
            SELECT COUNT(*) as total 
            FROM ProcessedRouteDetails prd
            INNER JOIN UserDevice ud ON prd.GUID = ud.GUID
            INNER JOIN Users u ON ud.UserID = u.id
            WHERE u.id = '${id}'
        `}), // totalCount,
        // Already excludes the deleted ones
        context.data.execute({sql: `
            SELECT
              prd.id,
              prd.StartTime,
              prd.StopTime,
              prd.AverageSpeed,
              prd.Modality,
              prd.TotalDistance / 1000 TotalDistance,
              prd.FromDescription,
              prd.ToDescription,
              (
                SELECT STRING_AGG(Name, ', ')
                FROM Gamezone
                WHERE CHARINDEX(id, prd.CrossoverGamezones) > 0
              ) as GamezoneName,
              prd.CheckEvaded,
              prd.SmilesEarnedByDistance,
              prd.SmilesEarnedByPois,
              prd.PoisHit,
              prd.TripID,
              prd.BadgesEarned,
              (CASE
                  WHEN prd.CheckEvaded = 0
                    THEN 'Not in a campaign'
                  WHEN prd.BadgesEarned LIKE '%{type:"campaign"}%'
                    THEN 'Successful Campaign route'
                  ELSE 'Failed Campaign route'
                END
              ) as CampaignRouteResult,
              prd.ProcessedState
            FROM ProcessedRouteDetails prd
              INNER JOIN UserDevice ud ON prd.GUID = ud.GUID
              INNER JOIN Users u ON ud.UserID = u.id
            WHERE u.id = '${id}'
        `})
    ]).then(
        data => ({ status:200, payload:{
            success:    true,
            count:      data[0][0].total,
            data:       data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table ${TableName}`
        }})
    );
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
 function toResponse(res){
     return (result) => {
         res.status(result.status).contentType(ContentType).send(result.payload);
     }
 };

const cms = {
    get: function (req, res)
    {
        let context    = req.azureMobile;
        let userId     = req.query.userId;
        let offset     = parseInt(req.query.offset);
        let pageSize   = parseInt(req.query.pageSize);
        let orderBy    = req.query.orderBy;

        if (userId) {
            getById(context, offset, pageSize, orderBy, userId).then( toResponse(res) );
        } else {
            res.status(200).contentType(ContentType).send({
                success: false,
                error: `Failed to get User Routes - No User ID defined.`
            });
        }
    }
};

module.exports = cms;
