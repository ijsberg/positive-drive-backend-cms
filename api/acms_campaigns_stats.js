"use strict";
const verbose = true;
const AbstractStats = require("../share/abstractStats");
const ContentType   = 'application/json';
const DefaultStat   = {
    total:           0,
    perfectRoute:    0,
    perfectModality: 0,
    perfectDistance: 0,
    failedPolygon:   0
};

class CampaignsStats extends AbstractStats{
    constructor(){
        super(DefaultStat, 'Campaigns Stats')
    }

    buildGetByIdQuery(id, todayDate, pastDate) {
        return `
            SELECT
              CONVERT(date, route.RouteDate,1)       date,
              COUNT(route.RouteState               ) total,
              SUM(IIF(route.RouteState = 0, 1, 0))   perfectRoute,
              SUM(IIF(route.RouteState = 1, 1, 0))   perfectModality,
              SUM(IIF(route.RouteState = 2, 1, 0))   perfectDistance,
              SUM(IIF(route.RouteState = 3, 1, 0))   failedPolygon
            FROM CampaignRouteLog route
              INNER JOIN CampaignUserLink      userlink             ON route.CampaignLinkID          = userlink.id
              INNER JOIN Campaigns             campaign             ON userlink.CampaignID           = campaign.id
            WHERE
              route.RouteDate > '${pastDate}' AND
              route.RouteDate < '${todayDate}' AND
              campaign.id     = '${id}'
            GROUP BY CONVERT(date, route.RouteDate, 1)
            ORDER BY date DESC
            `
    }
}

const statsBuilder = new CampaignsStats();

/**
 * Processes the Http response to be sent
 *
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}

const cms = {
    get: function(req, res){
        let context    = req.azureMobile;
        let id         = req.query.id;
        let startDate  = req.query.startDate;
        let dayRange   = parseInt(req.query.dayRange);

        statsBuilder.getStats(context, id, dayRange, startDate).then( toResponse(res) );
    }
};

module.exports = cms;
