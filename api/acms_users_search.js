"use strict";
const verbose = true;
const ContentType   = 'application/json';
const DefaultPageSize = 20;
const DefaultOffset   =  0;

function getSearch(context, offset, pageSize, query){
    let searchByCampaign = false;
    let whereFilters = [
        'usr.deleted=0'
    ];

    if( query.searchFor && query.searchFor.trim().length > 0 ){
        whereFilters.push(' (' + [
            `usr.FirstName LIKE '%${query.searchFor.trim()}%'`,
            `usr.LastName  LIKE '%${query.searchFor.trim()}%'`,
            `usr.Email     LIKE '%${query.searchFor.trim()}%'`,
            `usr.Provider  LIKE '%${query.searchFor.trim()}%'`
        ].join(' OR ') + ') ')
    }
    if( query.gamezoneId && query.gamezoneId.trim().length > 0 ){
        whereFilters.push(`gz.id = '${query.gamezoneId.trim()}'`)
    }
    if( query.campaignId && query.campaignId.trim().length > 0 ){
        searchByCampaign = true;
        whereFilters.push(`cul.CampaignId = '${query.campaignId.trim()}'`)
    }

    return Promise.all([
        context.data.execute({
            sql: `
            SELECT COUNT(*) as total 
            FROM Users AS usr
              INNER JOIN UserDevice AS ud ON usr.id = ud.UserID
              LEFT JOIN Gamezone AS gz ON usr.LastGamezoneID = gz.id
            ${ searchByCampaign ?
              ' LEFT JOIN CampaignUserLink AS cul ON cul.UserID = usr.id ' : ''
            }
            WHERE ${whereFilters.join(' AND ')}
            `
        }), // totalCount,
        // Already excludes the deleted ones
        context.data.execute({sql: `
            SELECT 
              usr.id                                   as id,
              CONCAT(usr.FirstName, ' ', usr.LastName) as name,
              usr.Email                                as email,
              gz.Name                                  as lastGamezoneName,
              usr.Provider                             as provider,
              ud.Model                                 as model,
              ud.RoutenetID                            as routenetId
            FROM Users AS usr
              INNER JOIN UserDevice AS ud ON usr.id = ud.UserID
              LEFT JOIN Gamezone AS gz ON usr.LastGamezoneID = gz.id
          ${ searchByCampaign ?
              ' LEFT JOIN CampaignUserLink AS cul ON cul.UserID = usr.id ' : ''
            }
            WHERE ${whereFilters.join(' AND ')}
            ORDER BY name
            OFFSET ${offset} ROWS
            FETCH NEXT ${pageSize} ROWS ONLY
        `})
    ]).then(
        data => ({ status:200, payload:{
            success: true,
            count  : data[0][0].total,
            data   : data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table Campaign`
        }})
    );
}
/**
 * Processes the Http response to be sent
 *
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}

const cms = {
    post: function(req, res){
        let context    = req.azureMobile;
        let query      = req.body;
        let offset     = parseInt(req.query.offset)   || DefaultOffset;
        let pageSize   = parseInt(req.query.pageSize) || DefaultPageSize;

        getSearch(context, offset, pageSize, query).then(toResponse(res));
    }
};

module.exports = cms;
