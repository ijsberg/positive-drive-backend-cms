
"use strict";
const verbose = true;
const DefaultPageSize = 20;
const DefaultOffset   =  0;
const DefaultOrderBy  = 'createdAt';
const TableName       = 'Campaigns';
const ContentType     = 'application/json';

function getList(context, offset, pageSize, orderBy){
    offset   = offset   || DefaultOffset;
    pageSize = pageSize || DefaultPageSize;
    orderBy  = orderBy  || DefaultOrderBy;

    return Promise.all([
        context.data.execute({sql: `
            SELECT COUNT(*) as total 
            FROM ${TableName} 
            WHERE deleted = 0
        `}), // totalCount,
        // Already excludes the deleted ones
        context.data.execute({sql: `
            SELECT
              cp.id          Id,
              cp.Name        Name,
              cp.Modalities  Modalities,
              cp.ActiveTimes ActiveTimes,
              cp.Type        Type,
              cp.StartDate   StartDate,
              gz.Name        GamezoneName
            FROM ${TableName}  cp
              INNER JOIN Gamezone gz
              ON cp.GamezoneLink = gz.id WHERE cp.deleted = 0
            ORDER BY cp.${DefaultOrderBy}  
        `})
    ]).then(
        data => ({ status:200, payload:{
            success: true,
            count  : data[0][0].total,
            data   : data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table ${TableName}`
        }})
    );
}

function getById(context, id){
    if( !id ){
        return ({ status: 500, payload: {
            success: false,
            error: `Failed to get item from table ${TableName} - no Id defined'`
        }})
    }
    return context.tables(TableName).where({ id: id }).read().then(
        results => {
            if( results.length === 0 ){
                return { status : 404, payload: {
                    success: false,
                    error  : `Item with Id '${id}' not found in table '${TableName}'`
                }}
            }else{
                return { status: 200, payload: {
                    success: true,
                    data   : results[0]
                }}
            }
        },
        error   => ({ status: 500, payload: {
            success: false,
            error: `Failed to query table '${TableName}' for Id '${id}'`
        }})
    );
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}
const cms = {
    get: function (req, res)
    {
        let context    = req.azureMobile;
        let campaignId = req.query.campaignId;
        let offset     = parseInt(req.query.offset);
        let pageSize   = parseInt(req.query.pageSize);
        let orderBy    = req.query.orderBy;

        (campaignId ? getById(context, campaignId) : getList(context, offset, pageSize, orderBy))
            .then( toResponse(res) );
    }
};

module.exports = cms;
