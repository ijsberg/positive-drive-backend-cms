
"use strict";
const verbose = true;
const DefaultPageSize = 20;
const DefaultOffset   =  0;
const ContentType     = 'application/json';

function getById(context, id, offset, pageSize){
    if( !id ){
        return Promise.resolve({ status: 500, payload: {
            success: false,
            error: `Failed to get Gamezones Routes - No Id defined.`
        }});
    }

    return Promise.all([
        context.data.execute({sql: `
            SELECT count(*) as total
            FROM (
              SELECT DISTINCT Name, Company, GamezoneID
              FROM Prizes_V2
              WHERE deleted = 0 AND PrizeType >= 0 AND GamezoneID = '${id}'
            ) as pz
        `}),
        context.data.execute({sql: `
           SELECT
              pr.id,
              pz.name,
              pz.company,
              pr.displayName,
              pz.available,
              pz.wonUsers,
              pr.logoURL,
              pr.imageURL
            FROM
              (
                SELECT
                  pr.Name                              name,
                  pr.Company                           company,
                  pr.GamezoneID                        gamezoneId,
                  SUM(IIF(pr.Available = 1, 1, 0))     available,
                  SUM(IIF(pr.WonUserID IS NULL, 0, 1)) wonUsers,
                  STRING_AGG(Codes, ';')               codes
                FROM Prizes_V2 pr
                WHERE pr.deleted = 0 AND pr.PrizeType >= 0 AND pr.GamezoneID = '${id}'
            
                GROUP BY pr.Name, pr.Company, pr.GamezoneID
              ) as pz
              OUTER APPLY (
                SELECT TOP 1
                  id                          as id,
                  ImageURL                    as imageURL,
                  LogoURL                     as logoURL,
                  DisplayName                 as displayName,
                  CONVERT(date, updatedAt, 1) as updateAt
                FROM Prizes_V2
                WHERE Name = pz.Name AND Company = pz.Company AND GamezoneID = pz.GamezoneID
              ) as pr
            ORDER BY pr.displayName DESC
            OFFSET ${offset} ROWS
            FETCH NEXT ${pageSize} ROWS ONLY
        `})
    ]).then(
        data => ({ status:200, payload:{
            success: true,
            count:   data[0][0].total,
            data:    data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get query GamezonePrizes`
        }})
    );
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    };
}

const cms = {
    get: function (req, res)
    {
        let context    = req.azureMobile;
        let gamezoneId = req.query.id;
        let offset     = parseInt(req.query.offset)   || DefaultOffset;
        let pageSize   = parseInt(req.query.pageSize) || DefaultPageSize;

        getById(context, gamezoneId, offset, pageSize).then( toResponse(res) );
    }
};

module.exports = cms;
