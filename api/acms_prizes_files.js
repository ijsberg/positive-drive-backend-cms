
"use strict";
const verbose = true;
const ContentType = 'application/json';
const MultiParty  = require('multiparty');
const Azure       = require('azure');
const UUID        = require('uuid');

function updatePrize(context, id, type, path){
    let item = {
        id: id,
    };
    if( type === 'logo'){
        item.LogoURL = 'https://pddata.blob.core.windows.net/'+path;
    }else{
        item.ImageURL = 'https://pddata.blob.core.windows.net/'+path;
    }
    return context.tables('Prizes_V2').update( item ).then(
        results => ({ status: 200, payload: {
            success: true,
            data   : item
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Failed to update item on Table '${TableName}' with data '${JSON.stringify(item)}'`
        }})
    );
}


/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}
const cms = {
    post: function(req, res) {
        let context     = req.azureMobile;
        let prizeId     = req.query.id;
        let type        = req.query.type;
        let form        = new MultiParty.Form();
        let blobService = Azure.createBlobService();

        if( !prizeId || !type ){
            return toResponse(res)({status: 500, payload: {message: `Invalid PrizeId/Type - ${prizeId}/${type}`}});
        }
        form.on('part', (part) => {
           if( part.filename ){
               let size = part.byteCount;
               let name = UUID.v4() + '.' + part.filename.split('.').pop();

               blobService.createBlockBlobFromStream('prizes', `${name}`, part, size, function(error){
                   if( error ){
                       console.error(error);
                       return toResponse(res)({status: 500, payload: {message: 'Failed push binary to blob storage - ' + part.filename}})
                   }else{
                       updatePrize(context, prizeId, type, `prizes/${name}`).then(toResponse(res));
                   }
               })
           } else {
               form.handlePart(part)
           }
        });
        form.parse(req);
    },
};

module.exports = cms;
