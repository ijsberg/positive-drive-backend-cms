
"use strict";
const verbose = true;
const TableName = 'ProcessedRouteDetails';
const ContentType = 'application/json';

function getList(context){
    return Promise.all([
        context.data.execute({sql: `
            SELECT COUNT(*) total
            FROM dbo.ProcessedRouteDetails AS p
            INNER JOIN dbo.UserDevice AS u ON p.GUID = u.GUID
            INNER JOIN dbo.Users AS ut ON u.UserID = ut.id
            WHERE p.deleted = 0
            AND p.ProcessedState = 2
        `}), // totalCount,
        // Already excludes the deleted ones
        context.data.execute({sql: `
            SELECT
                p.id,
                p.createdAt,
                p.StartTime,
                p.StopTime,
                u.GUID,
                u.UserID,
                ut.FirstName,
                p.AverageSpeed,
                p.TotalDistance/1000 AS TotalDistance,
                p.TripID,
                p.ProcessedState
            FROM dbo.ProcessedRouteDetails AS p
            INNER JOIN dbo.UserDevice AS u ON p.GUID = u.GUID
            INNER JOIN dbo.Users AS ut ON u.UserID = ut.id
            WHERE p.deleted = 0
            AND p.ProcessedState = 2
            ORDER BY p.StartTime DESC
        `})
    ]).then(
        data => ({ status:200, payload:{
            success: true,
            count  : data[0][0].total,
            data   : data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table ${TableName}`
        }})
    );
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}
const cms = {
    get: function (req, res)
    {
        let context = req.azureMobile;

        getList(context).then( toResponse(res) );
    }
};

module.exports = cms;
