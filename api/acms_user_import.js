
"use strict";
const verbose = true;
const DefaultPageSize = 20;
const DefaultOffset   =  0;
const DefaultOrderBy  = 'createdAt';
const TableName       = 'CampaignUserLink';
const ContentType     = 'application/json';

function setUsers(context, body, campaignId) {
    let promisseArray = [];

    body.forEach( (el) => {
        if (el['Campaign code']) {
            let inviteInfo = {
                kenteken: el['kenteken'],
                'Ref nivo A t/m D': el['Ref nivo A t/m D'],
                'ref nivo E': el['ref nivo E'],
                'VOOR-LET-NAT-P': el['VOOR-LET-NAT-P'],
                'VOOR-VOEG-NAT-P': el['VOOR-VOEG-NAT-P'],
                'STRAAT-NAAM-ADR': el['STRAAT-NAAM-ADR'],
                'HUIS-NR-ADR': el['HUIS-NR-ADR'],
                'HUIS-TV-ADR': el['HUIS-TV-ADR'],
                'POSTC-N-ADR': el['POSTC-N-ADR'],
                'POSTC-A-ADR': el['POSTC-A-ADR'],
                'WOONPL-NAAM-ADR': el['WOONPL-NAAM-ADR'],
                'Kenteken 2 op huisadres': el['Kenteken 2 op huisadres'],
                'Kenteken 3 op huisadres': el['Kenteken 3 op huisadres']
            };

            let insObject = {
                CampaignID: campaignId,
                FirstName: el['NAAM-AANSPR'],
                Accepted: 0,
                InviteCode: el['Campaign code'],
                Banned: 0,
                InviteInfo: JSON.stringify(inviteInfo)
            };

            promisseArray.push(context.data.execute({sql: `
                IF NOT EXISTS
                    (   SELECT 1
                        FROM CampaignUserLink
                        WHERE InviteCode = '${insObject.InviteCode}'
                    )
                BEGIN
                    INSERT INTO CampaignUserLink (CampaignID, FirstName, Accepted, InviteCode, Banned, InviteInfo)
                    VALUES (@CampaignID, @FirstName, @Accepted, @InviteCode, @Banned, @InviteInfo)
                END
            `,
            parameters:[
                { name: 'CampaignID', value: insObject.CampaignID },
                { name: 'FirstName', value: insObject.FirstName },
                { name: 'Accepted', value: insObject.Accepted },
                { name: 'InviteCode', value: insObject.InviteCode },
                { name: 'Banned', value: insObject.Banned },
                { name: 'InviteInfo', value: insObject.InviteInfo }
            ]}));
        }
    });

    return Promise.all(promisseArray).then(
        data => ({ status:200, payload:{
            success: true
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to import users`
        }})
    );
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}
const cms = {
    post: function (req, res)
    {
        let context    = req.azureMobile;
        let campaignId = req.query.campaignId;
        let body       = req.body;

        if (!body || !campaignId) {
            res.status(200).contentType(ContentType).send({
                success: false,
                error: `Invalid request - missing parameters`
            });
        } else {
            setUsers(context, body, campaignId)
                .then( toResponse(res) );
        }

    }
};

module.exports = cms;
