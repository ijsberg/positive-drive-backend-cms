
"use strict";
const verbose = true;
const DefaultPageSize = 20;
const DefaultOffset   =  0;
const DefaultOrderBy  = 'createdAt';
const TableName       = 'Users';
const ContentType     = 'application/json';

function getById(context, offset, pageSize, orderBy, id){
    offset   = offset   || DefaultOffset;
    pageSize = pageSize || DefaultPageSize;
    orderBy  = orderBy  || DefaultOrderBy;

    return Promise.all([
        context.data.execute({sql: `
            SELECT COUNT(*) as total FROM GamezoneUserLink gul
            INNER JOIN Users u ON gul.UserID = u.id
            INNER JOIN Gamezone g ON gul.GamezoneID = g.id
            WHERE gul.UserID = '${id}'
        `}), // totalCount,
        // Already excludes the deleted ones
        context.data.execute({sql: `
            SELECT
                gul.GamezoneID,
                g.Name,
                g.DistancePerSmile_walk,
                g.DistancePerSmile_bike,
                g.DistancePerSmile_car,
                g.DistancePerSmile_bus
            FROM GamezoneUserLink gul
            INNER JOIN Users u ON gul.UserID = u.id
            INNER JOIN Gamezone g ON gul.GamezoneID = g.id
            WHERE gul.UserID = '${id}'
        `}) // totalCount,
        // context.tables(TableName).skip(offset).take(pageSize).orderBy(orderBy).read()
    ]).then(
        data => ({ status:200, payload:{
            success:    true,
            count:      data[0][0].total,
            data:       data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table ${TableName}`
        }})
    );
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
};

const cms = {
    get: function (req, res)
    {
        let context    = req.azureMobile;
        let userId     = req.query.userId;
        let offset     = parseInt(req.query.offset);
        let pageSize   = parseInt(req.query.pageSize);
        let orderBy    = req.query.orderBy;

        if (userId) {
            getById(context, offset, pageSize, orderBy, userId).then( toResponse(res) );
        } else {
            res.status(200).contentType(ContentType).send({
                success: false,
                error: `Failed to get User Gamezones - No User ID defined.`
            });
        }
    }
};

module.exports = cms;
