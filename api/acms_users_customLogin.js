'use strict';
const verbose = true;
const auth     = require('azure-mobile-apps/src/auth');
const identity = require('aspnet-identity-pw');
const logger   = require('../share/logger.js')("api/acms_users_customLogin");

var api = 
{
    post: function (req, res)
    {
        let context = req.azureMobile;
        let sign = auth(context.configuration.auth).sign;

        let data = req.query;
        if ( !data || Object.keys(data).length === 0){
            data = req.body;
        }

        let userTable = context.tables("AspNetUsers");
        userTable.where({UserName: data.email}).read().then(function (users) {
            if (users.length !== 1) {
                logger.print("warn", "Invalid email for " + data.email);
                return res.status(401).send("Incorrect username or password");
            }
            if (identity.validatePassword(data.password, users[0].PasswordHash)) {
                let user = users[0];
                let token = sign({sub: user.Id});

                res.status(200).send({success: true, data: {
                    id           : user.Id,
                    token        : token,
                    email        : user.Email,
                    userName     : user.UserName,
                    resetPassword: user.PasswordReset
                }});
            } else {
                logger.print("warn", "Invalid password for " + data.email);
                res.status(401).send("Incorrect username or password");
            }
        })
        .catch(function(ex) {
            logger.print("error", ex);
            res.status(500).send("Invalid Request");
        });
    }
};

module.exports = api;
