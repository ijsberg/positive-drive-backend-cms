
"use strict";
const verbose = true;
const ContentType = 'application/json';

function getList(context){
    return context.data.execute({
        sql: `
            SELECT
              count(*)       as count,
              dev.AppVersion as appVersion
            FROM Users usr
              INNER JOIN UserDevice dev
              ON usr.id = dev.UserID
            WHERE usr.deleted = 0 AND dev.AppVersion like '%.%.%'
            GROUP BY dev.AppVersion
            ORDER BY AppVersion
    `}).then(
        data => ({ status:200, payload:{
            success: true,
            count  : data.length,
            data   : data
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get query User App Statistics`
        }})
    );
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}
const cms = {
    get: function (req, res)
    {
        let context    = req.azureMobile;
        getList(context).then( toResponse(res) );
    }
};

module.exports = cms;
