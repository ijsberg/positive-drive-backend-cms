
"use strict";
const verbose = true;
const DefaultPageSize = 20;
const DefaultOffset   =  0;
const DefaultOrderBy  = 'createdAt';
const TableName       = 'RawRouteSummary';
const ContentType     = 'application/json';

function processData(data){
    return new Promise((resolve, reject) => {
        let userArray = [];
        // let routeArray = [];

        data[0].forEach( item => {
            userArray.push( "'" + item.UserID + "'" );
        });

        // data[1].forEach( item => {
        //     routeArray.push( "'" + item.RawRouteSummaryID + "'" );
        // });

        let stringUserArray = userArray.join(', ');
        // let stringRouteArray = userArray.join(', ');

        resolve([
            stringUserArray
        ]);
    });
};

function doQuery(context, arrays, id){
    return Promise.all([
        context.data.execute({sql: `
            SELECT COUNT(*) as total
            FROM Users
            WHERE id IN ( ${arrays[0]} )
        `}),
        context.data.execute({sql: `
            SELECT TOP 100 *
            FROM Users
            WHERE id IN ( ${arrays[0]} )
            ORDER BY Users.updatedAt
        `}),
        context.data.execute({sql: `
            SELECT COUNT(*) as total
            FROM ProcessedRouteDetails prd
            INNER JOIN UserDevice ud ON prd.GUID = ud.GUID
            INNER JOIN Users u ON ud.UserID = u.id
            WHERE prd.CrossoverGamezones LIKE '%${id}%'
        `}),
        context.data.execute({sql: `
            SELECT TOP 100
            	prd.id,
            	prd.StartTime,
            	prd.StopTime,
            	prd.CrossoverGamezones,
            	ud.GUID,
            	ud.UserID,
            	u.FirstName,
            	prd.AverageSpeed,
            	prd.Modality,
            	prd.TotalDistance / 1000 TotalDistance,
            	prd.TripID,
            	prd.FromDescription,
            	prd.ToDescription,
            	prd.Pois,
            	prd.SmilesEarnedByDistance,
            	prd.SmilesEarnedByPois,
            	prd.PoisHit
            FROM ProcessedRouteDetails prd
            INNER JOIN UserDevice ud ON prd.GUID = ud.GUID
            INNER JOIN Users u ON ud.UserID = u.id
            WHERE prd.CrossoverGamezones LIKE '%${id}%'
        `})
    ]).then(
        data => ({ status:200, payload:{
            success : true,
            usersCount  : data[0][0].total,
            users       : data[1],
            routesCount : data[2][0].total,
            routes      : data[3]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table ${TableName}`
        }})
    );
}

function getById(context, id){
    if( !id ){
        return ({ status: 500, payload: {
            success: false,
            error: `Failed to get Gamezones Users - No Id defined.`
        }})
    }

    return Promise.all([
        context.data.execute({sql: `
            SELECT UserID
            FROM GamezoneUserLink
            WHERE GamezoneID = '${id}'
        `})
    ]).then(
        data => {
            return processData(data)
                .then(arrays => doQuery(context, arrays, id)
                    .then(result => {
                        return result;
                    })
                )
                .catch(err => {
                    console.log(err);
                });
        },
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table ${TableName}`
        }})
    );
};

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res, result){
    res.status(result.status).contentType(ContentType).send(result.payload);
};

const cms = {
    get: function (req, res, next)
    {
        let context    = req.azureMobile;
        let gamezoneId = req.query.gamezoneId;
        let offset     = parseInt(req.query.offset);
        let pageSize   = parseInt(req.query.pageSize);
        let orderBy    = req.query.orderBy;

        ( gamezoneId ? getById(context, gamezoneId)
            .then( data => toResponse(res, data) ) : toResponse(res, { status: 500, payload: {
            success: false,
            error: `Failed to get Gamezone Users - No Id defined.`
        }}) );
    }
};

module.exports = cms;
