"use strict";
const verbose = true;
const ContentType   = 'application/json';
const DefaultPageSize = 20;
const DefaultOffset   =  0;

function getSearch(context, offset, pageSize, query){

    let whereFilters = [
        'pr.deleted = 0',
        'pr.PrizeType >= 0'
    ];

    if( query.searchFor && query.searchFor.trim().length > 0 ){
        whereFilters.push(`pr.Name LIKE '%${query.searchFor.trim()}%'`);
    }
    if( query.gamezoneId && query.gamezoneId.trim().length > 0 ){
        whereFilters.push(`pr.GamezoneID = '${query.gamezoneId.trim()}'`)
    }

    return Promise.all([
        context.data.execute({
            sql: `
            SELECT count(*) as total
            FROM (
              SELECT DISTINCT Name, Company, GamezoneID
              FROM Prizes_V2 pr
              WHERE ${whereFilters.join(' AND ')}
            ) as pz
            `
        }), // totalCount,
        // Already excludes the deleted ones
        context.data.execute({sql: `
            SELECT pr.*, pz.*, gz.Name AS gamezoneName
            FROM
              (
                SELECT
                  pr.Name                              name,
                  pr.Company                           company,
                  pr.GamezoneID                        gamezoneId,
                  SUM(IIF(pr.Available = 1, 1, 0))     available,
                  SUM(IIF(pr.WonUserID IS NULL, 0, 1)) wonUsers,
                  STRING_AGG(Codes, ';')               codes
                FROM Prizes_V2 pr
                WHERE ${whereFilters.join(' AND ')}
                GROUP BY pr.Name, pr.Company, pr.GamezoneID
              ) as pz
              OUTER APPLY (
                SELECT TOP 1 
                    id                          as id, 
                    ImageURL                    as imageURL, 
                    LogoURL                     as logoURL, 
                    DisplayName                 as displayName,
                    CONVERT(date, updatedAt, 1) as updateAt
                FROM Prizes_V2
                WHERE Name = pz.Name AND Company = pz.Company AND GamezoneID = pz.GamezoneID
              ) as pr
            INNER JOIN Gamezone gz ON gz.id = pz.GamezoneID
            ORDER BY updateAt DESC
            OFFSET ${offset} ROWS
            FETCH NEXT ${pageSize} ROWS ONLY
        `
        })
    ]).then(
        data => ({ status:200, payload:{
            success: true,
            count  : data[0][0].total,
            data   : data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table Prizes`
        }})
    );
}

/**
 * Processes the Http response to be sent
 *
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}

const cms = {
    post: function(req, res){
        let context    = req.azureMobile;
        let query      = req.body;
        let offset     = parseInt(req.query.offset)   || DefaultOffset;
        let pageSize   = parseInt(req.query.pageSize) || DefaultPageSize;

        getSearch(context, offset, pageSize, query).then(toResponse(res));
    }
};

module.exports = cms;
