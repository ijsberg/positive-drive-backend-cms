"use strict";
const verbose = true;
const ContentType   = 'application/json';

function getSearch(context, query){

    let whereFilters = [
        'deleted=0'
    ];

    if( query.searchFor && query.searchFor.trim().length > 0 ){
        whereFilters.push(`Name LIKE '%${query.searchFor.trim()}%'`)
    }

    return Promise.all([
        context.data.execute({
            sql: `
            SELECT COUNT(*) as total 
            FROM Gamezone
            WHERE ${whereFilters.join(' AND ')}
            `
        }), // totalCount,
        // Already excludes the deleted ones
        context.data.execute({sql: `
            SELECT
                id,
                updatedAt,
                version,
                deleted,
                Name,
                DistancePerSmile_walk / 1000 AS DistancePerSmile_walk,
                DistancePerSmile_bike / 1000 AS DistancePerSmile_bike,
                DistancePerSmile_car / 1000 AS DistancePerSmile_car,
                DistancePerSmile_bus / 1000 AS DistancePerSmile_bus,
                Geometry,
                Priority
            FROM Gamezone
            WHERE ${whereFilters.join(' AND ')}
            ORDER BY updatedAt DESC
        `})
    ]).then(
        data => ({ status:200, payload:{
            success: true,
            count  : data[0][0].total,
            data   : data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table Campaign`
        }})
    );
}
/**
 * Processes the Http response to be sent
 *
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}

const cms = {
    post: function(req, res){
        let context    = req.azureMobile;
        let query      = req.body;

        getSearch(context, query).then(toResponse(res));
    }
};

module.exports = cms;
