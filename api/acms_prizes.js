
"use strict";
const verbose = true;
const TableName       = 'Prizes_V2';
const ContentType     = 'application/json';
const UUID            = require('uuid');
const DefaultPageSize  = 20;
const DefaultOffset    =  0;

function getList(context, offset, pageSize){
    return Promise.all([
        context.data.execute({sql: `
            SELECT count(*) as total
            FROM (
              SELECT DISTINCT Name, Company, GamezoneID
              FROM Prizes_V2
              WHERE deleted = 0 AND PrizeType >= 0
            ) as pz
        `}), // totalCount,
        // Already excludes the deleted ones
        context.data.execute({sql: `
            SELECT pr.*, pz.*, gz.Name AS gamezoneName
            FROM
              (
                SELECT
                  pr.Name                              name,
                  pr.Company                           company,
                  pr.GamezoneID                        gamezoneId,
                  SUM(IIF(pr.Available = 1, 1, 0))     available,
                  SUM(IIF(pr.WonUserID IS NULL, 0, 1)) wonUsers,
                  STRING_AGG(Codes, ';')               codes
                FROM Prizes_V2 pr
                WHERE pr.deleted = 0 AND pr.PrizeType >= 0
                GROUP BY pr.Name, pr.Company, pr.GamezoneID
              ) as pz
              OUTER APPLY (
                SELECT TOP 1 
                    id                          as id, 
                    ImageURL                    as imageURL, 
                    LogoURL                     as logoURL, 
                    DisplayName                 as displayName,
                    CONVERT(date, updatedAt, 1) as updateAt
                FROM Prizes_V2
                WHERE Name = pz.Name AND Company = pz.Company AND GamezoneID = pz.GamezoneID
              ) as pr
            INNER JOIN Gamezone gz ON gz.id = pz.GamezoneID
            ORDER BY updateAt DESC
            OFFSET ${offset} ROWS
            FETCH NEXT ${pageSize} ROWS ONLY
        `
        })
    ]).then(
        data => ({ status:200, payload:{
            success: true,
            count  : data[0][0].total,
            data   : data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table ${TableName}`
        }})
    );
}

function getById(context, id){
    if( !id ){
        return ({ status: 500, payload: {
            success: false,
            error: `Failed to get item from table ${TableName} - no Id defined'`
        }})
    }
    return context.tables(TableName).where({ id: id }).read().then(
        results => {
            if( results.length === 0 ){
                return { status : 404, payload: {
                    success: false,
                    error  : `Item with Id '${id}' not found in table '${TableName}'`
                }}
            }else{
                return { status: 200, payload: {
                    success: true,
                    data   : results[0]
                }}
            }
        },
        error   => ({ status: 500, payload: {
            success: false,
            error: `Failed to query table '${TableName}' for Id '${id}'`
        }})
    );
}

function create(context, item){

    let items = item.codes
        .split(';')
        .map  ( code => ({
            id:            UUID.v4(),
            Name:          item.name,
            LogoURL:       item.logoURL,
            ImageURL:      item.imageURL,
            PrizeType:     item.prizeType,
            MailType:      item.mailType,
            Company:       item.company,
            CompanyMail:   item.companyMail,
            StoreAddress:  item.storeAddress,
            PrizeSponsor:  item.prizeSponsor,
            ContactName:   item.contactName,
            Codes:         code,
            WinningNumber: -1,
            GamezoneID:    item.gamezoneId,
            WonUserID:     null,
            WonDate:       null,
            Claimed:       false,
            ClaimAddress:  item.claimAddress,
            Available:     true,
            DisplayName:   item.displayName
        }));


    return Promise.all( items.map(item => context.tables(TableName).insert(item)) ).then(
        result => ({ status: 200, payload: {
            success: true,
            data   : result
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Failed to create item on Table '${TableName}' with data '${JSON.stringify(item)}'`
        }})
    );
}
//
// function update(context, item){
//     return context.tables(TableName).update( item ).then(
//         results => ({ status: 200, payload: {
//             success: true,
//             data   : item
//         }}),
//         error => ({ status: 500, payload: {
//             success: false,
//             error: `Failed to update item on Table '${TableName}' with data '${JSON.stringify(item)}'`
//         }})
//     );
// }

function remove(context, id){
    if( !id ){
        return ({ status: 500, payload: {
            success: false,
            error: `Failed to delete item from table ${TableName} - no Id defined'`
        }})
    }
    return context.data.execute({sql: `
        UPDATE ${TableName} SET deleted = 1 WHERE id in (
          SELECT pr1.id
          FROM ${TableName} as pr1 INNER JOIN (
            SELECT Name, Company, GamezoneID
            FROM ${TableName}
            WHERE id = '${id}'
          ) AS pr2 ON pr1.Name = pr2.Name AND pr1.Company = pr2.Company AND pr1.GamezoneID = pr2.GamezoneID
          WHERE Available = 1
        )
    `}).then(
        results => ({ status: 200, payload: {
            success: true
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Failed to delete item from Table '${TableName}' with id '${id}'`
        }})
    );
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}
const cms = {
    get: function (req, res)
    {
        let context    = req.azureMobile;
        let prizeId    = req.query.id;
        let offset     = parseInt(req.query.offset  ) || DefaultOffset;
        let pageSize   = parseInt(req.query.pageSize) || DefaultPageSize;

        (prizeId ? getById(context, prizeId) : getList(context, offset, pageSize)).then( toResponse(res) );
    },
    post: function(req, res) {
        let context = req.azureMobile;
        let item    = req.body;

        create(context, item).then( toResponse(res) );
    },
    delete: function(req, res) {
        let context = req.azureMobile;
        let prizeId = req.query.id;

        remove(context, prizeId).then( toResponse(res) );
    }
};

module.exports = cms;
