// public ActionResult Shuffle(string gamezoneID, int chance)
// {
//     var db = DatabaseManager.databaseEntities();
//
//     var prizes = db.Prizes_V2.Where(p => p.deleted == false && p.GamezoneID == gamezoneID && String.IsNullOrEmpty(p.WonUserID)).ToList();
//     var curWinNumber = db.GamezonePrizeInfoes.Where(g => g.GamezoneID == gamezoneID).ToList()[0].CurrentPrizeNumber;
//     var totalChances = prizes.Count * chance;
//
//     Random r = new Random(DateTime.Now.Millisecond);
//     foreach(var prize in prizes)
//     {
//         var winningNumber = 0;
//         do
//         {
//             winningNumber = (int)Math.Floor((double)curWinNumber + r.Next(0, totalChances));
//         }
//         while (prizes.Where(p => p.WinningNumber == winningNumber).ToList().Count > 0);
//         prize.WinningNumber = winningNumber;
//
//         db.Entry(prize).State = EntityState.Modified;
//     }
//
//     db.SaveChanges();
//
//     return Redirect("Index");
// }

"use strict";
const verbose = true;
const ContentType  = 'application/json';
const logger       = require('../share/logger.js')("api/acms_prizes_shuffle");

/**
 *
 * @param prizes Array<{WinningNumber:number}>
 * @param curWinNumber number
 * @param chances number
 */
function shufflePrizes(prizes, curWinNumber, chances){

    let totalChances = prizes.length * chances;

    logger.info(`Shuffling '${prizes.length}' prizes`);
    prizes.forEach( prize => {
        let winningNumber = 0;
        do{
            winningNumber = Math.floor( curWinNumber + Math.floor(Math.random() * totalChances) );
        }while( prizes.some( p => p.WinningNumber === winningNumber) );
        prize.WinningNumber = winningNumber;
    });
    return prizes;
}

function getPrizes(context, gamezoneId){
    logger.info(`Getting Prizes to Shuffle`);
    return Promise.all([
        context.data.execute({sql: `
            SELECT id, WinningNumber
            FROM Prizes_V2
            Where deleted = 0 AND GamezoneID = '${gamezoneId}' AND WonUserID <> ''
        `}),
        context.data.execute({sql:`
            SELECT CurrentPrizeNumber as curWinNumber
            FROM GamezonePrizeInfo
            Where GamezoneID = '${gamezoneId}'    
        `})
    ]).then( data => ({prizes: data[0], curWinNumber: (data[1] || [{curWinNumber: 0}])[0].curWinNumber}));
}

function savePrizes(context, data){
    logger.info(`Saving Shuffled Prizes`);
    return context.data.execute({sql: `
        ${data.map( item => ` UPDATE Prizes_V2 SET WinningNumber = ${item.WinningNumber} WHERE id = '${item.id}'; `).join(" \n ")}
    `})
    .then(
        _ => {
            logger.info(`Finished`);
            return {status:200, payload:{success:true}};
        },
        error => {
            logger.error(`Error while saving prizes - ${error.message}`, error);
            return {status:500, payload:{success:false, message: `Error while saving prizes - ${error.message}`}};
        }
    )
    ;
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}
const prizes = {
    get: function (req, res)
    {
        let context    = req.azureMobile;
        let gamezoneId = req.query.gamezoneId;
        let chances    = parseInt(req.query.chances);

        if(!gamezoneId || isNaN(chances) ){
            return toResponse(res)({status:500, payload:{message: `Invalid GamezoneId/chances '${gamezoneId}/${chances}'`}} )
        }

        getPrizes ( context, gamezoneId)
            .then ( data  => shufflePrizes(data.prizes, data.curWinNumber, chances))
            .then ( data  => savePrizes(context, data))
            .catch( error => ({status:500, payload:{message: `Failed with Error: ${error.message}'`}} ))
            .then ( toResponse(res) )

        ;
    }
};

module.exports = prizes;
