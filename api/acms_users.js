
"use strict";
const verbose = true;
const DefaultPageSize = 20;
const DefaultOffset   =  0;
const DefaultOrderBy  = 'createdAt';
const ContentType     = 'application/json';

function getList(context, offset, pageSize, orderBy){
    offset   = offset   || DefaultOffset;
    pageSize = pageSize || DefaultPageSize;
    orderBy  = orderBy  || DefaultOrderBy;

    return Promise.all([
        context.data.execute({sql: `
        SELECT COUNT(*) as total 
        FROM Users 
        WHERE deleted=0`}), // totalCount,
        // Already excludes the deleted ones
        context.data.execute({sql: `
            SELECT 
              usr.id                                   as id,
              CONCAT(usr.FirstName, ' ', usr.LastName) as name,
              usr.Email                                as email,
              gz.Name                                  as lastGamezoneName,
              usr.Provider                             as provider,
              ud.Model                                 as model,
              ud.RoutenetID                            as routenetId
            FROM Users AS usr
              INNER JOIN UserDevice AS ud ON usr.id = ud.UserID
              LEFT JOIN Gamezone AS gz ON usr.LastGamezoneID = gz.id
            WHERE usr.deleted = 0
            ORDER BY name
            OFFSET ${offset} ROWS
            FETCH NEXT ${pageSize} ROWS ONLY
        `})
    ]).then(
        data => ({ status:200, payload:{
            success: true,
            count  : data[0][0].total,
            data   : data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table ${TableName}`
        }})
    );
}

function getById(context, id){
    if( !id ){
        return ({ status: 500, payload: {
            success: false,
            error: `Failed to get item from table ${TableName} - no Id defined'`
        }})
    }
    return context.data.execute({sql:`
            SELECT
                u.id,
                u.FirstName,
                u.LastName,
                u.Gender,
                u.Birthday,
                u.Email,
                u.Street,
                u.StreetAddition,
                u.StreetNR,
                u.City,
                u.ZipCode,
                u.Phone,
                u.Organisation,
                u.LastLogin,
                u.createdAt,
                ud.RoutenetID,
                ud.Model,
                ud.SystemVersion,
                ud.AppVersion,
                cul.InviteInfo,
                cul.InviteCode,
                cul.updatedAt campaignActiveSince
            FROM Users AS u
            INNER JOIN UserDevice AS ud ON ud.UserID = u.id
            LEFT JOIN CampaignUserLink AS cul ON cul.UserID = u.id
            WHERE u.id = '${id}'
            AND u.deleted = 'false'
        `}).then(
        results => {
            if( results.length === 0 ){
                return { status : 404, payload: {
                    success: false,
                    error  : `Item with Id '${id}' not found in table '${TableName}'`
                }}
            }else{
                return { status: 200, payload: {
                    success: true,
                    data   : results[0]
                }}
            }
        },
        error   => ({ status: 500, payload: {
            success: false,
            error: `Failed to query table '${TableName}' for Id '${id}'`
        }})
    );
}


function update(context, item){
    return context.tables('Users').update( item ).then(
        results => ({ status: 200, payload: {
            success: true,
            data   : item
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Failed to update item on Table 'Users' with data '${JSON.stringify(item)}'`
        }})
    );
}

function create(context, item){
    return Promise.resolve({
        status: 500, payload: {
            success:false,
            data: 'Endpoint not supported yet'
        }});
    // item.id = UUID.v4();
    // return context.tables('Users').insert(item).then(
    //     result => ({ status: 200, payload: {
    //         success: true,
    //         data   : result
    //     }}),
    //     error => ({ status: 500, payload: {
    //         success: false,
    //         error: `Failed to create item on Table '${TableName}' with data '${JSON.stringify(item)}'`
    //     }})
    // );
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}
const cms = {
    get: function (req, res)
    {
        let context    = req.azureMobile;
        let userId     = req.query.userId;
        let offset     = parseInt(req.query.offset);
        let pageSize   = parseInt(req.query.pageSize);
        let orderBy    = req.query.orderBy;

        (userId ? getById(context, userId) : getList(context, offset, pageSize, orderBy))
            .then( toResponse(res) );
    },
    post: function(req, res) {
        let context = req.azureMobile;
        let item    = req.body;

        (item.id ? update(context, item) : create(context, item)).then( toResponse(res) );
    }
};

module.exports = cms;
