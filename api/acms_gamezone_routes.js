
"use strict";
const verbose = true;
const DefaultPageSize = 20;
const DefaultOffset   =  0;
const DefaultOrderBy  = 'createdAt';
const TableName       = 'RawRouteSummary';
const ContentType     = 'application/json';

function getById(context, id, offset, pageSize){
    if( !id ){
        return Promise.resolve({ status: 500, payload: {
            success: false,
            error: `Failed to get Gamezones Routes - No Id defined.`
        }});
    }

    return Promise.all([
        context.data.execute({sql: `
            SELECT COUNT(*) as total
            FROM ProcessedRouteDetails prd
            WHERE prd.CrossoverGamezones LIKE '%${id}%'
        `}),
        context.data.execute({sql: `
            SELECT
              FORMAT(prd.StartTime,'dd/MM/yyyy hh:mm') startDate,
              FORMAT(prd.StopTime,'dd/MM/yyyy hh:mm')  stopDate,
              prd.Modality                             modality,
              prd.AverageSpeed                         averageSpeed,
              prd.TotalDistance/1000                   totalDistance,
              prd.SmilesEarnedByDistance               smilesEarnedByDistance,
              prd.FromDescription                      fromDescription,
              prd.ToDescription                        toDescription,
              prd.SmilesEarnedByPois                   smilesEarnedByPois,
              prd.PoisHit                              poisHits
            FROM ProcessedRouteDetails prd
            WHERE prd.CrossoverGamezones LIKE '%${id}%'
            ORDER BY prd.StartTime
            OFFSET ${offset} ROWS
            FETCH NEXT ${pageSize} ROWS ONLY
        `})
    ]).then(
        data => ({ status:200, payload:{
            success: true,
            count:   data[0][0].total,
            data:    data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table ${TableName}`
        }})
    );
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    };
}

const cms = {
    get: function (req, res)
    {
        let context    = req.azureMobile;
        let gamezoneId = req.query.id;
        let offset     = parseInt(req.query.offset)   || DefaultOffset;
        let pageSize   = parseInt(req.query.pageSize) || DefaultPageSize;
        let orderBy    = req.query.orderBy;

        getById(context, gamezoneId, offset, pageSize).then( toResponse(res) );
    }
};

module.exports = cms;
