
"use strict";
const verbose = true;
const DefaultPageSize  = 20;
const DefaultOffset    =  0;
const DefaultOrderBy   = 'createdAt';
const TableName        = 'Users';
const ContentType      = 'application/json';

function getById(context, id, offset, pageSize){
    if( !id ){
        return Promise.resolve({ status: 500, payload: {
            success: false,
            error: `Failed to get Gamezones Users - No Id defined.`
        }})
    }

    return Promise.all([
        context.data.execute({sql: `
            SELECT count(*) total
            FROM GamezoneUserLink gul
              INNER JOIN Users usr
              ON gul.UserID = usr.id AND usr.deleted = 0
            WHERE GamezoneID = '${id}'
        `}),
        context.data.execute({sql: `
            SELECT
              CONCAT(usr.FirstName, ' ', usr.LastName) as name,
              usr.Email                                as email,
              usr.Provider                             as provider,
              g.Name                                   as lastGamezone
            FROM GamezoneUserLink gul
              INNER JOIN Users usr
              ON gul.UserID = usr.id AND usr.deleted = 0
              LEFT JOIN Gamezone g
              on usr.LastGamezoneID = g.id
            WHERE GamezoneID = '${id}'
            ORDER BY usr.createdAt
            OFFSET ${offset} ROWS
            FETCH NEXT ${pageSize} ROWS ONLY
        `})
    ]).then(
        data => ({ status:200, payload:{
            success: true,
            count  : data[0][0].total,
            data   : data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table ${TableName}`
        }})
    );
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    };
}

const cms = {
    get: function (req, res)
    {
        let context    = req.azureMobile;
        let gamezoneId = req.query.id;
        let offset     = parseInt(req.query.offset  ) || DefaultOffset;
        let pageSize   = parseInt(req.query.pageSize) || DefaultPageSize;

        getById(context, gamezoneId, offset, pageSize).then( toResponse(res) );
    }
};

module.exports = cms;
