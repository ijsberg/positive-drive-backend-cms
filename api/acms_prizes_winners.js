
"use strict";
const verbose = true;
const TableName       = 'Prizes_V2';
const ContentType     = 'application/json';
const DefaultPageSize  = 20;
const DefaultOffset    =  0;

function getWinnerListById(context, id, offset, pageSize){
    if( !id ){
        return ({ status: 500, payload: {
            success: false,
            error: `Failed to get item from table ${TableName} - no Id defined'`
        }})
    }
    return Promise.all([
        context.data.execute({sql: `
            SELECT count(*) as total
            FROM Prizes_V2 as pr1 INNER JOIN (
              SELECT DISTINCT Name, Company, GamezoneID
              FROM Prizes_V2
              WHERE delete = 0 AND id = '${id}' AND WonUserID IS NOT NULL
            ) AS pr2 ON pr1.Name = pr2.Name AND pr1.Company = pr2.Company AND pr1.GamezoneID = pr2.GamezoneID
        `}), // totalCount,
        // Already excludes the deleted ones
        context.data.execute({sql: `
            SELECT
              us.id                                  id,
              CONCAT(us.FirstName, ' ', us.LastName) name,
              us.Email                               email,
              FORMAT(pr.WonDate, 'dd/MM/yyyy hh:mm') date,
              pr.ClaimAddress                        claimAddress,
              pr.Claimed                             claimed,
              pr.Codes                               codes,
              gz.Name                                gamezoneName
            FROM (
              SELECT WonUserID, ClaimAddress, Claimed, Codes, WonDate, DisplayName, pr1.Name, pr1.GamezoneID
              FROM Prizes_V2 as pr1 INNER JOIN (
                SELECT DISTINCT Name, Company, GamezoneID
                FROM Prizes_V2
                WHERE delete = 0 AND id = '${id}' AND WonUserID IS NOT NULL
              ) AS pr2 ON pr1.Name = pr2.Name AND pr1.Company = pr2.Company AND pr1.GamezoneID = pr2.GamezoneID
            ) as pr 
              INNER JOIN Users us ON us.id = pr.WonUserID
              INNER JOIN Gamezone gz ON gz.id = pr.GamezoneID
            ORDER BY name
            OFFSET ${offset} ROWS
            FETCH NEXT ${pageSize} ROWS ONLY
        `
        })
    ]).then(
        data => ({ status:200, payload:{
            success: true,
            count  : data[0][0].total,
            data   : data[1]
        }}),
        error => ({ status: 500, payload: {
            success: false,
            error: `Fail to get list for table ${TableName}`
        }})
    );
}

/**
 * Processes the Http response to be sent
 * @param res
 * @return {function(*)}
 */
function toResponse(res){
    return (result) => {
        res.status(result.status).contentType(ContentType).send(result.payload);
    }
}
const cms = {
    get: function (req, res)
    {
        let context    = req.azureMobile;
        let prizeId    = req.query.id;
        let offset     = parseInt(req.query.offset  ) || DefaultOffset;
        let pageSize   = parseInt(req.query.pageSize) || DefaultPageSize;

        getWinnerListById(context, prizeId, offset, pageSize).then( toResponse(res) );
    }
};

module.exports = cms;
