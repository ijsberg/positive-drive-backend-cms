'use strict';
const verbose = true;
const logger   = require('../share/logger.js')("api/acms_users_passwordReset");
const sendgrid = require('../share/sendgrid.js');
const identity = require('aspnet-identity-pw');

var api = 
{
    post: function (req, res)
    {
        let context = req.azureMobile;

        let data = req.query;
        if ( !data || Object.keys(data).length === 0){
            data = req.body;
        }
        let userEmail = data.Email;

        let userTable = context.tables("AspNetUsers");
        userTable.where({ Email: userEmail }).read().then(function(result)
        {
            if (result.length !== 1) {
                return res.status(200).send({success: false, data: {error: "Unknown user"}});
            }
            let user = result[0];
            user.PasswordHash  = generateRandomString(6);
            user.PasswordReset = 1;

            userTable.update(user).then(function() {
                var subject = "";
                var message = "";

                subject = "Password reset PositiveDrive";

                message = "Hello,<br/><br/>";
                message += "Your temporary password is '" + newPassword + "'<br/>";
                message += "Once you loging you'll be asked to enter a new password<br/><br/>";
                message += "Stay Positive!<br/>";
                message += "Positive Drive";

                sendgrid.sendMail(userEmail, process.env.SENDGRID_FROM, subject, message);
                res.status(200).send({success: true});
            }).catch(function(ex) {
                logger.print("error", ex);
                res.status(500).send(ex);
            });
        }).catch(function(ex) {
            logger.print("error", ex);
            res.status(500).send(ex);
        });
    }
};

function generateRandomString(len)
{
	let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    len = len || 32;
    for (let i = 0; i < len; i++){
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

module.exports = api;
