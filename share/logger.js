'use strict';

function hasValidData(item)
{
	return item !== undefined && item !== null;
}

module.exports = function(script)
{
	process.setMaxListeners(0);
	
	var winston = require('winston');
	var azureLogger = require('winston-azuretable').AzureLogger;
	
	var logger = new (winston.Logger)({
	  transports: [
	  	new (winston.transports.File)({
		  name    : 'error-logger',
		  filename: 'logger-error.log',
		  level   : 'error'
	    }),
  	    new (winston.transports.Console)({
  	      timestamp: function() {
  			  return new Date();
  	      },
  	      formatter: function(options) {
  	        // Return string will be passed to logger.
  			  return script + ' ' + options.timestamp().toISOString() +' '+ options.level.toUpperCase() +' '+ (undefined !== options.message ? options.message : '') + (options.meta && Object.keys(options.meta).length ? '\n\t'+ JSON.stringify(options.meta) : '' );
  	      }
  	    })
	  ]
	});
	
	logger.add(azureLogger, {
		account: process.env.AZURE_STORAGE_ACCOUNT, 
		key: process.env.AZURE_STORAGE_ACCESS_KEY, 
		level: "info", 
		tableName: process.env.LOG_TABLENAME,
        useDevStorage: !process.env.AZURE_STORAGE_ACCOUNT || !process.env.AZURE_STORAGE_ACCESS_KEY || !process.env.LOG_TABLENAME,
		silent: false
	});
	
	logger.print = function(level, msg, metadata, callback)
	{
		if (hasValidData(metadata))
		{
			if (hasValidData(callback))
			{
				logger.log(level, new Date().toISOString() + ' ' + script + ': ' + msg, metadata, callback);
			}
			else
			{
				logger.log(level, new Date().toISOString() + ' ' + script + ': ' + msg, metadata);
			}
		}
		else
		{
			logger.log(level, new Date().toISOString() + ' ' + script + ': ' + msg);
		}
	};
	
	logger.handleExceptions();
	
	return logger;
}
