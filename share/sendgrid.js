'use strict';

var verbose  = true;
var logger   = require('./logger.js')("shared/email/sendgrid");
var sendgrid = require('sendgrid')(process.env.SENDGRID_USERNAME, process.env.SENDGRID_PASSWORD);
var template = require('./template.js');

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

exports.sendMail = function(toAddress, fromAddress, subject, fullText, ignoreHeader)
{
	var finalHTML = template.header + fullText + template.footer;
	if (ignoreHeader)
		finalHTML = fullText;
	
	var email = new sendgrid.Email({
	    to: toAddress,
	    from: fromAddress,
		fromname: "PositiveDrive",
	    subject: subject,
	    html: finalHTML,
	});
	
	sendgrid.send(email, function(error, json)
	{
		if (!error)
		{
			if (verbose)
				logger.print("info", "E-Mail send");
		}
		else
		{
			logger.print('error',error);
		}
	});
};

exports.sendMailCC = function(toAddress, fromAddress, subject, fullText, cc, ignoreHeader)
{
	var finalHTML = template.header + fullText + template.footer;
	if (ignoreHeader)
		finalHTML = fullText;
		
	var email = new sendgrid.Email({
	    to: toAddress,
	    from: fromAddress,
		cc: cc,
		fromname: "PositiveDrive",
	    subject: subject,
	    html: finalHTML,
	});
	
	sendgrid.send(email, function(error, json)
	{
		if (!error)
		{
			if (verbose)
				logger.print("info", "E-Mail send");
		}
		else
		{
			logger.print('error',error);
		}
	});
};
