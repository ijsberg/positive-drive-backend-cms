'use strict';

const MomentRange     = require('moment-range');
const Moment          = MomentRange.extendMoment(require('moment'));
const DefaultDayRange = 15;
const DefaultDateFormat = 'YYYY/MM/DD';

class AbstractStats {
    constructor(defaultValue, statsLabel){
        this._defaultValue = defaultValue;
        this._statsLabel   = statsLabel;
    }

    _tryMoment(date){
        try{
            return Moment(date);
        }catch(ex){
            return Moment();
        }
    }

    _processData(todayDate, pastDate, data){

        let dateRange     = Moment.range([pastDate, todayDate]).by('days', { exclusive: false });
        let formattedData = data.map( item => Object.assign(item, {date: Moment(item.date).format(DefaultDateFormat)}) );

        return Array.from(dateRange)
            .reverse()
            .map( m => m.format(DefaultDateFormat) )
            .map( date => {
                if( formattedData.length > 0 && formattedData[0].date === date){
                    return formattedData.shift();
                }else{
                    return Object.assign({}, {date:date}, this._defaultValue )
                }
            });
    }

    _doQuery(context, todayDate, pastDate, sqlQuery){
        return context.data.execute({ sql:sqlQuery })
            .then( data => this._processData(todayDate, pastDate, data))
            .then(
                data => ({ status:200, payload:{
                    success: true,
                    data   : data
                }}),
                error => ({ status: 500, payload: {
                    success: false,
                    error: `Fail to collect statistics for ${this._statsLabel} : ${error.response}`
                }})
            );
    }

    /**
     * Gets the statistics
     *
     * @param context:Object
     * @param id:String
     * @param dayRange:String
     * @param startDate:Number
     * @return {*}
     */
    getStats(context, id, dayRange, startDate) {
        let pastDate;
        let todayDate;
        let sqlQuery;

        todayDate = this._tryMoment(startDate);
        pastDate  = Moment(todayDate).subtract(dayRange || DefaultDayRange, 'days');

        if( id ){
            sqlQuery  = this.buildGetByIdQuery(id, todayDate.format(DefaultDateFormat), pastDate .format(DefaultDateFormat))
        }else{
            sqlQuery  = this.buildGetQuery    ( todayDate.format(DefaultDateFormat), pastDate .format(DefaultDateFormat))
        }
        return this._doQuery(context, todayDate, pastDate, sqlQuery);
    }

    buildGetByIdQuery(id, todayDate, pastDate){
        throw Error("Missing Implementation");
    }

    /**
     *
     * @param todayDate:String
     * @param pastDate:String
     */
    buildGetQuery(todayDate, pastDate){
        throw Error("Missing Implementation");
    }
}

module.exports = AbstractStats;
