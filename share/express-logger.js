'use strict';

const onHeaders = require('on-headers');
const UUID      = require("uuid");

module.exports = function (logger, opts) {

    const blacklist = opts && opts.blacklist || [];
    const policy = opts && opts.policy;

    /**
     * Return the client address from the last IP address in X-Forwarded-For HTTP header. If not possible to
     * obtain it from X-Forwarded-For header, then return req.ip (however, this value typically is the load
     * balancer IP address and it does not provide any valuable information).
     *
     * @param {Object} req
     *    Express request
     * @return {String}
     *    Client IP address
     */
    function getClientIp(req) {
        let xff = req.get('x-forwarded-for');
        if (xff) {
            let ips = xff.split(',').map(function onIp(ip) {
                return ip.trim();
            });
            let ip = ips[ips.length - 1];
            if (ip) {
                return ip;
            }
        }
        return req.ip;
    }

    /**
     * Check if the request URL starts with any of the blacklist paths.
     *
     * @param {String} url
     *    Request url
     * @return {Boolean}
     *    True if the request URL is included in the blacklist.
     */
    function isUrlBlackedListed(url) {
        return blacklist.some(function (blackListUrl) {
            return url.indexOf(blackListUrl) === 0;
        });
    }

    return function loggingMiddleware(req, res, next) {
        let eventId = UUID.v4();
        if (!isUrlBlackedListed(req.originalUrl)) {
            let startTime = Date.now();
            if (policy === 'params') {
                let requestParams = {
                    requestClientIp: getClientIp(req),
                    requestMethod: req.method,
                    requestUrl: req.originalUrl
                };
                logger.info(requestParams, '%s Request: %s %s', eventId, req.method, req.originalUrl);
            } else {
                logger.info('%s Request from %s: %s %s', eventId, getClientIp(req), req.method, req.originalUrl);
            }

            onHeaders(res, function onResponse() {
                let duration = Date.now() - startTime;
                let location = res.get('location');
                if (policy === 'params') {
                    let responseParams = {
                        responseStatusCode: res.statusCode,
                        responseDuration: duration,
                        responseLocation: location
                    };
                    logger.info(responseParams, '%s Response with status %d', eventId, res.statusCode);
                } else {
                    if (location) {
                        logger.info('%s Response with status %d in %d ms. Location: %s', eventId, res.statusCode, duration, location);
                    } else {
                        logger.info('%s Response with status %d in %d ms.', eventId, res.statusCode, duration);
                    }
                }
            });
        }
        next();
    };

};
